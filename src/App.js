import 'react-native-gesture-handler';
import * as React from 'react';
import {SafeAreaView, Platform} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {UserProvider} from './store/UserContext';
import RootNavigator from './navigation/Navigation';
import {navigationRef, isReadyRef} from './navigation/NavigationRef';

const theme = {
  ...DefaultTheme,
  roundness: 5,
  colors: {
    ...DefaultTheme.colors,
    primary: '#0092bc',
    accent: '#f1c40f',
  },
};

export default function App() {
  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => {
        isReadyRef.current = true;
      }}>
      <PaperProvider theme={theme}>
        <UserProvider>
          <SafeAreaView style={{flex: 0, backgroundColor: 'white'}} />
          <RootNavigator />
        </UserProvider>
      </PaperProvider>
    </NavigationContainer>
  );
}
