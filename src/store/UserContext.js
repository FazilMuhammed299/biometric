import React, {useReducer, createContext, useMemo, useEffect} from 'react';
import {Platform} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {Snackbar} from 'react-native-paper';
import {initialState, authReducer, CONSTANT} from './UserReducer';

import * as NavigationRef from '../navigation/NavigationRef';

import PasscodeAuth from '@el173/react-native-passcode-auth';
import TouchID from 'react-native-touch-id';

const storeData = async value => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem('@userData', jsonValue);
  } catch (e) {
    // saving error
  }
};
export const UserContext = createContext();

export const UserProvider = props => {
  const [state, dispatch] = useReducer(authReducer, initialState);

  const [visible, setVisible] = React.useState(false);
  const [message, setMessage] = React.useState('');

  const showToast = msg => {
    setVisible(!visible);
    setMessage(msg);
  };

  const onDismissSnackBar = () => setVisible(false);

  const userContext = useMemo(
    () => ({
      handleBioMetric: optionalConfigObject => {
        return new Promise(async (resolve, reject) => {
          try {
            const type =
              Platform.OS === 'ios'
                ? await PasscodeAuth.isSupported(optionalConfigObject)
                : await TouchID.isSupported(optionalConfigObject);

            if (type) {
              let response =
                Platform.OS === 'ios'
                  ? await PasscodeAuth.authenticate('')
                  : await TouchID.authenticate('', optionalConfigObject);
              if (response) {
                showToast('Successfully Autheticated');

                resolve(response);
              } else {
                resolve(null);
                showToast('Something went wrong...');
              }

              return response;
            } else {
              NavigationRef.navigate('ErrorHandleScreen');
            }
          } catch (error) {
            let errorCode = error.message;

            if (errorCode == 'LAErrorUserCancel') {
              showToast('Authentication was canceled by the user');
            } else if (errorCode == 'LAErrorAuthenticationFailed') {
              showToast('Failed to provide valid credentials.');
            } else if (errorCode == 'LAErrorUserFallback') {
              showToast(
                'Authentication was canceled because the user tapped the fallback button (Enter Password)',
              );
            } else if (errorCode == 'LAErrorSystemCancel') {
              showToast(
                'Authentication was canceled by system—for example, if another application came to foreground while the authentication dialog was up.',
              );
            } else if (errorCode == 'LAErrorPasscodeNotSet') {
              showToast(
                'Authentication could not start because the passcode is not set on the device.',
              );
            } else if (errorCode == 'LAErrorTouchIDNotAvailable') {
              showToast(
                'Authentication could not start because Touch ID is not available on the device',
              );
            } else if (errorCode == 'LAErrorTouchIDNotEnrolled') {
              showToast(
                'Authentication could not start because Touch ID has no enrolled fingers.',
              );
            } else if (errorCode == 'RCTTouchIDUnknownError') {
              showToast('Could not authenticate for an unknown reason.');
            } else if (errorCode == 'RCTTouchIDNotSupported') {
              showToast('Device does not support Touch ID or Face ID.');
              props.navigation.navigate('ErrorHandleScreen');
            } else {
              showToast('Something went wrong hhhh');
            }

            reject(errorCode);

            return error.message;
          }
        });
      },

      getData: url => {
        return new Promise((resolve, reject) => {
          axios
            .post(url, {
              timeout: 10000,
              headers: {
                Accept: 'application/json',
              },
            })
            .then(res => {
              if (res) {
                let response = res.data;
                if (response.status) {
                  resolve(response);
                  return response;
                } else {
                  showToast('Something went wrong');
                  return response;
                }
              } else {
                showToast('Something went wrong');
              }
            })
            .catch(error => {
              showToast('Check your internet connection');
              resolve(null);
              reject(error);
            });
        });
      },

      saveLocation: data => {
        dispatch({
          type: CONSTANT.getLocation,
          payload: data,
        });
      },
    }),

    [state],
  );

  useEffect(() => {
    // Fetch the token from storage then navigate to our appropriate place
    const initialize = async () => {
      let userData;

      try {
        userData = await AsyncStorage.getItem('@userData');
      } catch (e) {}

      dispatch({
        type: 'RESTORE_TOKEN',
        state: userData ? JSON.parse(userData) : {},
      });
    };
    if (state.isLoading) {
      initialize();
    } else {
      storeData(state);
    }
  }, [state]);

  return (
    <UserContext.Provider value={{...userContext, ...state, showToast}}>
      {props.children}
      <Snackbar
        visible={visible}
        duration={3000}
        style={{borderRadius: 10}}
        onDismiss={onDismissSnackBar}>
        {message}
      </Snackbar>
    </UserContext.Provider>
  );
};
