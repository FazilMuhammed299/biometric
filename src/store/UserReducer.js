export const CONSTANT = {
  restore_data: 'RESTORE_TOKEN',
  getLocation: 'GETLOCATION',
  appStatus: 'APPSTATUS',
};

export const initialState = {
  isLoading: true,
  latitude: null,
  longitude: null,
  positionCoordinates: null,
};

export const authReducer = (state, action) => {
  console.log(state, action);
  switch (action.type) {
    case CONSTANT.restore_data:
      return {
        ...state,
        ...action.state,
        isLoading: false,
      };
    case CONSTANT.getLocation:
      return {
        ...state,
        latitude: action.payload.latitude,
        longitude: action.payload.longitude,
        positionCoordinates: action.payload.position,
      };
    case CONSTANT.screen:
      return {
        ...state,
        page: action.page,
      };

    default:
      return state;
  }
};

export default authReducer;
