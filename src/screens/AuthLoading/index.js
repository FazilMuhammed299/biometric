import React, {useContext, useEffect, useState, useRef} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Alert,
  Linking,
  Platform,
  AppState,
  PermissionsAndroid,
} from 'react-native';
import {ActivityIndicator} from 'react-native-paper';
import DeviceInfo from 'react-native-device-info';
import IntentLauncher, {IntentConstant} from 'react-native-intent-launcher';
import Geolocation from '@react-native-community/geolocation';
import {UserContext} from '../../store/UserContext';

let subscription;

const AuthLoading = props => {
  const {saveLocation, positionCoordinates} = useContext(UserContext);
  const [isAuth, setAuth] = useState(false);

  const appState = useRef(AppState.currentState);

  useEffect(() => {
    if (!isAuth) {
      getLocation();
      subscription = AppState.addEventListener('change', handleAppStateChange);
      return () => {
        subscription.remove();
      };
    }
  }, [isAuth]);

  const handleAppStateChange = nextAppState => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
      getLocation();
    }

    appState.current = nextAppState;
  };

  const getLocation = () => {
    const requestLocationPermission = async () => {
      if (Platform.OS === 'ios') {
        getOneTimeLocation();
      } else {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'Location Access Required',
              message: 'This App needs to Access your location',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            getOneTimeLocation();
          } else {
            console.log('Permission Denie');
            // setLocationStatus('Permission Denied');
          }
        } catch (err) {
          console.warn(err);
        }
      }
    };
    requestLocationPermission();
  };

  const getOneTimeLocation = () => {
    Geolocation.getCurrentPosition(
      //Will give you the current location
      position => {
        // getting the Longitude from the location json
        const currentLongitude = JSON.stringify(position.coords.longitude);

        //getting the Latitude from the location json
        const currentLatitude = JSON.stringify(position.coords.latitude);

        let data = {
          latitude: currentLatitude,
          longitude: currentLongitude,
          position: position.coords,
        };
        saveLocation(data);

        setAuth(true);

        props.navigation.navigate('Authentication');
      },
      error => {
        console.log(error, 'erooor');

        if (error.message == 'User denied access to location services.') {
          Alert.alert(
            'User denied access to location services.',
            'Please turn on Location always from Settings',
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Cicked.'),
                style: 'cancel',
              },
              {
                text: 'OK',
                onPress: () => {
                  if (Platform.OS === 'ios') {
                    Linking.openURL('app-settings:');

                    // setTimeout(() => {
                    //   RNRestart.Restart();
                    // }, 500);
                  } else {
                    let bundleID = DeviceInfo.getBundleId();

                    IntentLauncher.startActivity({
                      action: 'android.settings.APPLICATION_DETAILS_SETTINGS',
                      data: 'package:' + bundleID,
                    });

                    // setTimeout(() => {
                    //   RNRestart.Restart();
                    // }, 500);
                  }
                },
              },
            ],
            {cancelable: true},
          );
        }
        // setLocationStatus(error.message);
      },
      {
        enableHighAccuracy: false,
        timeout: 30000,
        maximumAge: 1000,
      },
    );
  };

  return (
    <View style={[styles.container]}>
      <ActivityIndicator size="large" color="#2ecc71" />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f4f4f4',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default AuthLoading;
