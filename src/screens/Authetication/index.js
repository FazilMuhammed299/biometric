import {platform} from 'os';
import React, {useState, useEffect, useContext, useCallback} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {UserContext} from '../../store/UserContext';

const Authentication = props => {
  const {showToast, handleBioMetric} = useContext(UserContext);
  const [cancelAuthetication, setAuthetication] = useState(false);

  const optionalConfigObject = {
    title: 'Authentication Required', // Android
    imageColor: '#e00606', // Android
    imageErrorColor: '#ff0000', // Android
    sensorDescription: 'Touch sensor', // Android
    sensorErrorDescription: 'Failed', // Android
    cancelText: 'Cancel', // Android
    fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
    unifiedErrors: false, // use unified error messages (default false)
    passcodeFallback: false, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
  };

  const biometricAction = () => {
    handleBioMetric(optionalConfigObject)
      .then(res => {
        if (res) {
          props.navigation.navigate('HomeScreen');
        } else {
          showToast('Something went wrong.......');
        }
      })
      .catch(err => {
        console.log(err, 'message');
        setAuthetication(true);
      });
  };

  useEffect(() => {
    biometricAction();
  }, []);

  return (
    <View style={[styles.container]}>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#000333',
        }}>
        {cancelAuthetication && (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              source={require('../../assets/padlock.png')}
              resizeMethod="auto"
              resizeMode="contain"
              style={{width: 60, height: 60}}
            />

            <Text
              style={{
                padding: 20,
                color: 'white',
                fontWeight: 'bold',
                fontSize: 22,
                textAlign: 'center',
              }}>
              App is Unlocked
            </Text>

            <Text
              style={{
                padding: 20,
                color: 'white',
                fontWeight: '800',
                fontSize: 16,
                textAlign: 'center',
              }}>
              Unlock with face ID or Touch ID to Open App
            </Text>

            <TouchableOpacity
              activeOpacity={0.8}
              style={{marginTop: 50}}
              onPress={() => {
                setAuthetication(!cancelAuthetication);
                biometricAction();
              }}>
              <Text
                style={{
                  padding: 20,
                  fontWeight: 'bold',
                  fontSize: 18,
                  color: 'steelblue',
                }}>
                Try Again
              </Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f4f4f4',
  },
  modal: {
    width: '70%',
    height: '30%',
    borderRadius: 10,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Authentication;

// import React, {Component} from 'react';
// import {Alert, StyleSheet, Text, TouchableHighlight, View} from 'react-native';

// import TouchID from 'react-native-touch-id';

// import PasscodeAuth from 'react-native-passcode-auth';

// export default class FingerPrint extends Component {
//   constructor() {
//     super();

//     this.state = {
//       biometryType: null,
//     };
//   }

//   componentDidMount() {
// const optionalConfigObject = {
//   title: 'Authentication Required', // Android
//   imageColor: '#e00606', // Android
//   imageErrorColor: '#ff0000', // Android
//   sensorDescription: 'Touch sensor', // Android
//   sensorErrorDescription: 'Failed', // Android
//   cancelText: 'Cancel', // Android
//   fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
//   unifiedErrors: true, // use unified error messages (default false)
//   passcodeFallback: true, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
// };

//     TouchID.isSupported().then(biometryType => {
//       this.setState({biometryType});
//     });
//   }

//   render() {
//     return (
//       <View style={styles.container}>
//         <TouchableHighlight
//           style={styles.btn}
//           onPress={this.clickHandler}
//           underlayColor="#0380BE"
//           activeOpacity={1}>
//           <Text
//             style={{
//               color: '#fff',
//               fontWeight: '600',
//             }}>
//             {`Authenticate with ${this.state.biometryType}`}
//           </Text>
//         </TouchableHighlight>
//       </View>
//     );
//   }

//   clickHandler() {
//     const optionalConfigObject = {
//       title: 'Authentication Required', // Android
//       imageColor: '#e00606', // Android
//       imageErrorColor: '#ff0000', // Android
//       sensorDescription: 'Touch sensor', // Android
//       sensorErrorDescription: 'Failed', // Android
//       cancelText: 'Cancel', // Android
//       fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
//       unifiedErrors: true, // use unified error messages (default false)
//       passcodeFallback: true, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
//     };

//     TouchID.isSupported()
//       .then(authenticate)
//       .catch(error => {
//         Alert.alert('TouchID not supported');
//       });
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: '#F5FCFF',
//   },
//   btn: {
//     borderRadius: 3,
//     marginTop: 200,
//     paddingTop: 15,
//     paddingBottom: 15,
//     paddingLeft: 15,
//     paddingRight: 15,
//     backgroundColor: '#0391D7',
//   },
// });

// function authenticate() {
//   const optionalConfigObject = {
//     title: 'Authentication Required', // Android
//     imageColor: '#e00606', // Android
//     imageErrorColor: '#ff0000', // Android
//     sensorDescription: 'Touch sensor', // Android
//     sensorErrorDescription: 'Failed', // Android
//     cancelText: 'Cancel', // Android
//     fallbackLabel: 'Show Passcode', // iOS (if empty, then label is hidden)
//     unifiedErrors: true, // use unified error messages (default false)
//     passcodeFallback: true, // iOS - allows the device to fall back to using the passcode, if faceid/touch is not available. this does not mean that if touchid/faceid fails the first few times it will revert to passcode, rather that if the former are not enrolled, then it will use the passcode.
//   };

//   return TouchID.authenticate(optionalConfigObject)
//     .then(success => {
//       Alert.alert('Authenticated Successfully');
//     })
//     .catch(error => {
//       console.log(error);
//       Alert.alert(error.message);
//     });
// }
