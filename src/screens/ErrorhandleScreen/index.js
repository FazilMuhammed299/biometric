import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const ErrorHandleScreen = props => {
  return (
    <View style={[styles.container]}>
      <Text
        style={{
          textAlign: 'center',
          fontSize: 18,
          fontWeight: '800',
          paddingHorizontal: 20,
        }}>
        This device doesn't support any kind of Biometric Authentication
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f4f4f4',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default ErrorHandleScreen;
