import React, {useContext, useEffect, useState, useCallback} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import {UserContext} from '../../store/UserContext';
import {GoogleAPI} from '../../store/Keys';

const HomeScreen = props => {
  const {latitude, longitude, getData, positionCoordinates} =
    useContext(UserContext);
  const [locationName, setLocationName] = useState('');

  useEffect(() => {
    getData(
      `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${GoogleAPI}`,
    ).then(res => {
      console.log(res.results[0].formatted_address, 'resss');
      setLocationName(res.results[0].formatted_address);
    });
  }, []);

  return (
    <View style={[styles.container]}>
      <MapView
        style={{flex: 1}}
        provider={PROVIDER_GOOGLE}
        initialRegion={{
          latitude: positionCoordinates.latitude,
          longitude: positionCoordinates.longitude,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}>
        <Marker
          title={locationName}
          description={`lat: ${latitude}\nlon: ${longitude}\ntimestamp ${Math.round(
            +new Date() / 1000,
          )}`}
          coordinate={{
            latitude: positionCoordinates.latitude,
            longitude: positionCoordinates.longitude,
          }}>
          <Image
            resizeMethod="auto"
            resizeMode="contain"
            style={{width: 40, height: 40}}
            source={require('../../assets/marker.png')}
          />
        </Marker>
      </MapView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f4f4f4',
  },
});

export default HomeScreen;
