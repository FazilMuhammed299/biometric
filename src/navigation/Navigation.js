import React, {useContext} from 'react';
// import {createStackNavigator} from '@react-navigation/stack';

import {createNativeStackNavigator} from '@react-navigation/native-stack';

import AuthLoading from '../screens/AuthLoading';
import Authentication from '../screens/Authetication';
import HomeScreen from '../screens/HomeScreen';
import ErrorHandleScreen from '../screens/ErrorhandleScreen';
const Stack = createNativeStackNavigator();

function App() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="AuthLoading" component={AuthLoading} />
      <Stack.Screen name="Authentication" component={Authentication} />
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
      <Stack.Screen name="ErrorHandleScreen" component={ErrorHandleScreen} />
    </Stack.Navigator>
  );
}

export default App;
